﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurnSystem : MonoBehaviour
{

    float _time;
    GameManager _gameManager;
    TurnAction _turnAction;
    //Vector3 _tuenActionPosition;

    // Use this for initialization
    void Start()
    {
        _time = Time.deltaTime;
        _gameManager = GameManager.Settings;
        _turnAction = FindObjectOfType<TurnAction>();
        //_tuenActionPosition = _turnAction.transform.position;
        StartCoroutine(InitGame());
    }

    IEnumerator InitGame()
    {
        yield return new WaitForEndOfFrame();
    }

    // Update is called once per frame
    void Update()
    {
        GameStatus.Status status = _gameManager.GameStatus;

        switch (status)
        {
            case GameStatus.Status.playerTurn:
                break;
            case GameStatus.Status.enemyTurn:
                StartCoroutine(EnemyAIRealtime());
                break;
            case GameStatus.Status.waitForTurnEnd:
                break;
            default:
                break;
        }

        //float delTime = Time.deltaTime;
        //_time += delTime;
        //if (_time > 6.0f)
        //{
        //    Debug.Log("test");



        //    _time -= 6.0f;
        //}
    }

    // 當玩家結束回合時的整體行動
    // 包含了一些過場效果以及敵人的AI
    IEnumerator TurnRound()
    {
        _gameManager.GameStatus = GameStatus.Status.waitForTurnEnd;

        /* 玩家在點擊技能卡片的時候 不計算在回合內 */
        // 進入此區域代表玩家點選了回合結束 或者系統觸發回合結束的flag

        // 一段回合結束的演出
        // 敵人的行動
        yield return StartCoroutine(EnemyAI());
        yield return StartCoroutine(WaitUI(2.0f));
        // 回合結束的場地效果發動

        /* 回合判定結束 */
        Debug.Log("test after 2 seconds TurnRound");
        _gameManager.GameStatus = GameStatus.Status.playerTurn;
        //_turnAction.GetComponent<Button>().interactable = true;
        BaseUtil.GameObjectVisable(_turnAction.GetComponent<Button>().gameObject);

        //_turnAction.GetComponent<Button>().gameObject.transform.position = _tuenActionPosition;

        yield return 0;
    }

    IEnumerator EnemyAI()
    {
        /* 每個行動之間最好稍微間格0.3秒左右 */
        // 首先計算敵人打算採取的行動
        // 敵人開始行動
        // 一段行動動畫
        // 參數變動
        // 參數變動影響的動畫表現

        yield return 0;
        //enemyScript.isWaitPlayer = true;
    }

    IEnumerator EnemyAIRealtime()
    {
        _gameManager.GameStatus = GameStatus.Status.enemyTurnRunning;
        /* 每個行動之間最好稍微間格0.3秒左右 */
        // 首先計算敵人打算採取的行動 得到一個技能
        MonsterSkillInheritance monsterSkill =  _gameManager.GetEnemy().MonsterAI.EnemyAITurn();
        // 將敵人的行動埋入行動等待槽
        SkillColdDown skillColdDown = FindObjectOfType<SkillColdDown>();
        skillColdDown.PutSkill(monsterSkill.SkillColdDown, monsterSkill);
        // 改變畫面的行動預測文字

        yield return null;
        //enemyScript.isWaitPlayer = true;
    }

    IEnumerator WaitUI(float t)
    {
        yield return new WaitForSeconds(t);
    }

}
