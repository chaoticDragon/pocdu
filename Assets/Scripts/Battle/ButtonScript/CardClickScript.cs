﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CardClickScript : MonoBehaviour
{

    void Awake()
    {

        EventTrigger.Entry entry = new EventTrigger.Entry();// 定義需要绑定的事件類型。並設定回調函式
        entry.eventID = EventTriggerType.PointerClick;// 設定事件類型
        entry.callback = new EventTrigger.TriggerEvent();// 設定回調函式
        UnityAction<BaseEventData> callback = new UnityAction<BaseEventData>(Hello);
        entry.callback.AddListener(callback);

        EventTrigger trigger = transform.gameObject.AddComponent<EventTrigger>();
        trigger.triggers = new List<EventTrigger.Entry>();// 實體化 delegates
        trigger.triggers.Add(entry);// 添加事件觸發，記錄到 GameObject 的事件觸發元件
    }

    public void Hello(BaseEventData data)
    {


        string st = data.selectedObject.name;
        GameManager game = GameManager.Settings;
        SkillAdapter skillAdapter = game.GetSkillAdapter();
        SkillGeneric skill = null;
        int selectSkill = -1;
        string SliderName = null;
        SkillColdDownEvent skillColdDownEvent = null;

        //print("game null?" + (game.GetEnemy() == null));
        float _hp = GameManager.Settings.GetEnemy().HP;


        switch (st)
        {
            case "ButtonCard01":
                skill = skillAdapter.SkillList[0];
                SliderName = "CardSlider01";
                selectSkill = 0;
                break;
            case "ButtonCard02":
                skill = skillAdapter.SkillList[1];
                SliderName = "CardSlider02";
                selectSkill = 1;
                break;
            case "ButtonCard03":
                skill = skillAdapter.SkillList[2];
                SliderName = "CardSlider03";
                selectSkill = 2;
                break;
            case "ButtonCard04":
                skill = skillAdapter.SkillList[3];
                SliderName = "CardSlider04";
                selectSkill = 3;
                break;
            case "ButtonCard05":
                skill = skillAdapter.SkillList[4];
                SliderName = "CardSlider05";
                selectSkill = 4;
                break;
            //case "ButtonCard05":
            //    _hp += 20;
            //    print("ou click ButtonCard05");
            //    GameManager.Settings.GetEnemy().HP = _hp;
            //    print("now hp: " + GameManager.Settings.GetEnemy().HP);
            //    break;
            default:
                print("non select but print name: selectedObject.name: " + st);
                break;

        }

        if (skill != null)
        {

            // todo 增加一段是否能使用該技能的戰鬥判斷邏輯 比如技能尚未準備好攻擊時當前位置 剩餘魔力 異常狀態等
            // 若有發生判定且有通過 比如因為異常狀態而無法使用 則加上一段敘述
            // 敘述可能寫在這裡或者是異常狀態class自帶

            skillColdDownEvent = new SkillColdDownEvent();
            skillColdDownEvent.skill = skill;
            skillColdDownEvent.SliderName = SliderName;
            // 消耗該卡片並且重新抽卡
            BattleUtil.SkillUseAndRebuild(selectSkill);
            // 開始卡片cold down
            skillAdapter.SendSkillColdDownEvent(skillColdDownEvent);

            // 演出特效
            FindObjectOfType<EffectPlayer>().DoAnimator();
            // 增加對話框顯示使用了什麼技能的文字敘述蓋住卡片區域提示玩家卡片正在使用不可以繼續點
            // TODO feture 增加距離變更的UI示意eventhanlder

            // 實際運作卡片
            skill.Action();
        }
        //bool isButtonEventUsed = data.used;
        //data.selectedObject.gameObject.SetActive(false);

    }

    public class SkillColdDownEvent : EventArgs
    {
        public string SliderName;
        public SkillGeneric skill;
    }
}
