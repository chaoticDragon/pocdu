﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * 畫面中央的 重整態勢按鈕
 * 效果是等待一段時間刷新所有卡片 但不能刷新負面卡片
 */ 
public class TurnAction : MonoBehaviour
{
    SkillColdDown skillColdDown;
    void Start()
    {
        Button btn = this.GetComponent<Button>();
        btn.onClick.AddListener(OnClick);
        skillColdDown = FindObjectOfType<SkillColdDown>();
    }

    private void OnClick()
    {
        // TODO 按鈕消失的特效聲
        Button btn = this.GetComponent<Button>();
        BaseUtil.GameObjectInvisable(btn.gameObject);
        //btn.interactable = false;
        //btn.gameObject.transform.position = Vector3.zero;
        //btn.gameObject.SetActive(false);

        GameManager _gameManager = GameManager.Settings;
        if (_gameManager.GameStatus == GameStatus.Status.playerTurn)
        {
            skillColdDown.TestSkill();
            _gameManager.GameStatus = GameStatus.Status.enemyTurn;
        }
    }

}
