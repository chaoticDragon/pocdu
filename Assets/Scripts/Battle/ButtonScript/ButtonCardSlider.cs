﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonCardSlider : MonoBehaviour, IButtonMono
{
    bool _isSkillActivate = false;
    float _coldDown = 0f;
    float _skillPrepareStart = 0f;

    Slider slider;

	void Start () {
        slider = GetComponent<Slider>();
        slider.interactable = false;

        SkillAdapter skillAdapter = GameManager.Settings.GetSkillAdapter();
        skillAdapter.SkillColdDown += this.OnSkillColdDown;
    }


    public void MonoUpdate()
    {
        if (_isSkillActivate)
        {
            _coldDown += Time.deltaTime * Time.timeScale;
            slider.value = 1f - (_coldDown / _skillPrepareStart);
            if (_coldDown >= _skillPrepareStart)
            {
                TrySkillActive();
            }
        }
    }

    private void TrySkillActive()
    {
        _isSkillActivate = false;
    }

    void OnSkillColdDown(object sender, CardClickScript.SkillColdDownEvent e)
    {
        //Debug.Log("on OnSkillColdDown! , name: ");
        string st = this.name;
        SkillGeneric skill = e.skill;
        //SkillAdapter skillAdapter = GameManager.Settings.GetSkillAdapter();

        if (skill != null)
        {
            if (st.Equals(e.SliderName))
            {
                _coldDown = 0f;
                _skillPrepareStart = skill.SkillColdDown;
                _isSkillActivate = true;
            }
        }
    }
}
