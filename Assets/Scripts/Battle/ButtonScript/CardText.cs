﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardText : MonoBehaviour
{

    Text text;

    void Awake()
    {
        text = GetComponent<Text>();
    }

    // Use this for initialization
    void Start()
    {
        SkillAdapter skillAdapter = GameManager.Settings.GetSkillAdapter();
        skillAdapter.SkillReCreated += this.OnSkillReCreated;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnSkillReCreated(object sender, EventArgs e)
    {
        string st = this.name;

        SkillAdapter skillAdapter = GameManager.Settings.GetSkillAdapter();
        ISkill skill = null;
        switch (st)
        {
            case "ButtonCardText01":
                skill = skillAdapter.SkillList[0];

                break;
            case "ButtonCardText02":
                skill = skillAdapter.SkillList[1];

                break;
            case "ButtonCardText03":
                skill = skillAdapter.SkillList[2];

                break;
            case "ButtonCardText04":
                skill = skillAdapter.SkillList[3];

                break;
            case "ButtonCardText05":
                skill = skillAdapter.SkillList[4];

                break;
        }

        if (skill != null)
        {
            text.text = skill.SkillName;
        }
    }
}
