﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class EffectPlayer : MonoBehaviour
{
    Image imageMonster;
    // Use this for initialization
    void Start()
    {
        imageMonster = GetComponent<Image>();
        //imageMonster.GetComponent<Image>().color = new Color32(255, 255, 225, 100);

        //DoAnimator();
    }

    public void DoAnimator()
    {
        //image.transform.position = new Vector3(46.1879f, -18.89571f, -6912f);
        Vector3 vector3 = new Vector3(96.31f, 101.01f, -1);
        Vector3 vectorScale = new Vector3(1.828401f, 1.897283f, 1);

        UnityEngine.Object pPrefab = Resources.Load("tor_0_0"); // note: not .prefab!
        GameObject pNewObject = (GameObject)GameObject.Instantiate(pPrefab, vector3, Quaternion.identity);
        //Debug.Log("has pNewObject? " + (pNewObject == null));
        pNewObject.transform.localScale = vectorScale;

        Animator animator = pNewObject.GetComponent<Animator>();
        //Debug.Log("has animator? " + (animator == null));
        animator.Play("tor_e_1");
        //Debug.Log("ani: " + animator.GetCurrentAnimatorClipInfo(0)[0].clip.ToString());
        StartCoroutine(WaitForAnimator(animator, pNewObject));
        iTween.ShakePosition(imageMonster.gameObject, iTween.Hash("x", 0.6f, "time", 0.5f, "delay", 0.3f));
        AudioClip audioClip = Resources.Load<AudioClip>("Audio/SE/SC_magic7");
        iTween.Stab(imageMonster.gameObject, audioClip, 0f);

        

        //UnityEngine.Object prefab = AssetDatabase.LoadAssetAtPath("Assets/Prefab/tor_0_0", typeof(Image));
        //Image player = Instantiate(prefab, vector3, Quaternion.identity) as Image;    }
    }

    private IEnumerator WaitForAnimator(Animator animator, GameObject pNewObject)
    {
        //do
        //{
        //    Debug.Log("ani: " + animator.GetCurrentAnimatorClipInfo(0)[0].clip.ToString());
        //    yield return null;
        //} while (animator.GetCurrentAnimatorClipInfo(0)[0].clip.ToString().Equals("tor_e_1"));

        //Debug.Log("ani: " + animator.GetCurrentAnimatorClipInfo(0)[0].clip.ToString());
        yield return new WaitForSeconds(0.5f);

        if (pNewObject != null)
        {
            UnityEngine.Object.Destroy(pNewObject);
            pNewObject = null;
        }
    }

    private IEnumerator WaitForAnimation(Animation animation)
    {
        do
        {
            yield return null;
        } while (animation.isPlaying);
    }
}
