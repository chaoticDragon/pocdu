﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleUtil {

    public static readonly float MIN_DAMAGE_RANGE = 0.95f;
    public static readonly float MAX_DAMAGE_RANGE = 1.05f;


    public static void SkillsInit()
    {
        GameManager.Settings.GetSkillAdapter().CreateSkillPool();

    }

    public static void SkillUseAndRebuild(int position)
    {
        SkillAdapter skillAdapter = GameManager.Settings.GetSkillAdapter();
        skillAdapter.SkillUse(position);
        skillAdapter.SkillNext(position);
    }

    public static float GetEnemyAttack(IMonsterSkill monsterSkill)
    {
       return GetEnemyAttack(monsterSkill.SkillName, monsterSkill.NearDamage, monsterSkill.MiddleDamage
            , monsterSkill.FarDamage, monsterSkill.BasePower);
    }

    public static float GetEnemyAttack(string skillName, float nearDamage, float middleDamage
        , float farDamage, float basePower)
    {
        GameManager gameManager = GameManager.Settings;
        Player player = gameManager.GetPlayer();
        float distanceDamage = 0f;
        switch (player.Distance)
        {
            case IPlayer.DistanceAssign.near:
                distanceDamage = nearDamage;
                break;
            case IPlayer.DistanceAssign.middle:
                distanceDamage = middleDamage;
                break;
            case IPlayer.DistanceAssign.away:
                distanceDamage = farDamage;
                break;
        }

        float trueDamage = distanceDamage * basePower / 100;
        Debug.Log("distanceDamage: " + distanceDamage);
        Debug.Log("basePower: " + basePower);
        Debug.Log("trueDamage: " + trueDamage);


        // 最終傷害增加一個浮動計算
        trueDamage = Random.Range(MIN_DAMAGE_RANGE, MAX_DAMAGE_RANGE) * trueDamage;
        Debug.Log("Range: " + trueDamage);

        return trueDamage;
        //player.Vitality -= trueDamage;
    }
	
}
