﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/**
 * 遊戲內常使用到的工具
 */
public class BaseUtil
{

    private static Dictionary<int, Vector3> PositionD = new Dictionary<int, Vector3>(20);

    public static void GameObjectInvisable(GameObject gameObject)
    {
        PositionD.Add(gameObject.GetInstanceID(), gameObject.transform.position);
        gameObject.transform.position = Vector3.zero;
    }

    public static void GameObjectVisable(GameObject gameObject)
    {
        Vector3 vector3 = PositionD[gameObject.GetInstanceID()];
        PositionD.Remove(gameObject.GetInstanceID());
        gameObject.transform.position = vector3;
    }
}
