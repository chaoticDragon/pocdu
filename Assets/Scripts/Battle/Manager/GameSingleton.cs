﻿using com.sunlab.PoCDU;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "Spacepuppy/Game Settings", order = int.MaxValue)]
public class GameManager : GameSettings

{
    SkillAdapter _skillAdapter;
    Enemy _enemy;
    Player _player;
    public GameStatus.Status GameStatus { get; set; }


    #region Singleton Access

    private static GameManager _instance;

    public static bool Initialized { get { return _instance != null; } }

    public static void Init()
    {
        _instance = GameSettings.GetGameSettings<GameManager>("GameManager");
        _instance.hideFlags = HideFlags.HideAndDontSave; // need use between sences
    }

    public static GameManager Settings
    {
        get
        {
            return _instance;
        }
    }

    #endregion

    #region Fields

    public string SomePropertyToSerialize;

    #endregion

    #region CONSTRUCTOR

    protected override void OnInitialized()
    {
        //do any initializing you may want for the game
        _enemy = CreateInstance<Enemy>();
        _player = CreateInstance<Player>();
        _skillAdapter = new SkillAdapter();
        GameStatus = global::GameStatus.Status.playerTurn;
    }

    #endregion


    public Player GetPlayer()
    {
        return _player;
    }

    public Enemy GetEnemy()
    {
        return _enemy;
    }

    public SkillAdapter GetSkillAdapter()
    {
        return _skillAdapter;
    }

}

public class GameStatus
{
    public enum Status
    {
        playerTurn, enemyTurn, waitForTurnEnd, enemyTurnRunning
    }
}
