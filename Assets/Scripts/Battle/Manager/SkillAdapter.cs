﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * 裝載目前戰鬥介面玩家可以用的技能
 * 
 * */
public class SkillAdapter  {

    private static int MAX_SKILL_NUMBER = 5;

    private List<SkillGeneric> _skillList;

    public event EventHandler SkillReCreated;
    public event EventHandler<CardClickScript.SkillColdDownEvent> SkillColdDown;


    public SkillAdapter()
    {
        _skillList = new List<SkillGeneric>(MAX_SKILL_NUMBER);
        for (int i = 0; i < MAX_SKILL_NUMBER; i++)
        {
            _skillList.Add(null);
        }
    }

    public List<SkillGeneric> SkillList
    {
        get
        {
            return _skillList;
        }
    }

    public void SkillUse(int position)
    {
        SkillList[position] = null;
    }

    /**
     * 抽一張新的卡牌
     */ 
    public void SkillNext(int position)
    {
        SkillList[position] = CreateSkill();
        InvokeSkillReCreated();
    }

    // 補充手牌
    public void CreateSkillPool()
    {
        SkillGeneric skill = null;
        for (int i = 0; i < MAX_SKILL_NUMBER; i++)
        {
            if ((skill = SkillList[i]) == null)
            {
                skill = CreateSkill();
                SkillList[i] = skill;
            }
        }

        InvokeSkillReCreated();
    }

    private SkillGeneric CreateSkill()
    {
        float randomF;
        randomF = Mathf.Floor(UnityEngine.Random.Range(0, 1f) * 3);
        switch ((int)randomF)
        {
            case 0:
                return new PlayerSkill_1(); ;
            case 1:
                return new PlayerSkill_2(); ;
            case 2:
                return new PlayerSkill_3(); ;
            default:
                break;
        }

        return new PlayerSkill_1(); ;
    }

    private void InvokeSkillReCreated()
    {
        if (SkillReCreated != null)
        {
            SkillReCreated(this, EventArgs.Empty);
        }
    }

    internal void SendSkillColdDownEvent(CardClickScript.SkillColdDownEvent skillColdDownEvent)
    {
        if (SkillColdDown != null)
        {
            SkillColdDown(this, skillColdDownEvent);
        }
    }
}
