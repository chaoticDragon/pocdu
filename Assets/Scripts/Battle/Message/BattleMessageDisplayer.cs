﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class BattleMessageDisplayer : MonoBehaviour {

    public Text text;
    private List<string> _messageArrayList;
    private string _messageString;

    public event EventHandler<MessageEventArgs> MessageDisplayEvent;

    private void Awake()
    {
        text = GetComponent<Text>();
        _messageString = "";
        _messageArrayList = new List<string>();
    }

    // Use this for initialization
    void Start () {
        ReflashText();
        Enemy enemy = GameManager.Settings.GetEnemy();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AddMessage(string s)
    {
        _messageArrayList.Add(s);
        ReflashText();
    }

    void PringMessage(string s)
    {
        _messageArrayList.Clear();
        _messageArrayList.Add(s);
        ReflashText();
    }

    private void ReflashText()
    {
        int length = _messageArrayList.Count();
        _messageString = "";
        for (int i = 0; i < length; i++)
        {
            if (i +5 > length)
            {
                _messageString += _messageArrayList[i];
                _messageString += "\n";
            }
        }

        text.text = _messageString;
    }

    void OnMessage(object sender, MessageEventArgs e)
    {
        string s =  e._s;
        AddMessage(s);
    }

    public class MessageEventArgs : System.EventArgs
    {
        public string _s
        {
            get
            {
                return _s;
            }

            set
            {
                _s = value;
                // 保留如果以後要在這裡進行換行判斷 都寫在這
            }
        }
    }
}
