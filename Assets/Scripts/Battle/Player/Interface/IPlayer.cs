﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IPlayer : ScriptableObject
{
    //protected float Vitality;
    //public float MaxVitality { get; set; }

    //// 意識
    //public float Conscious { get; set; }
    //public float MaxConscious { get; set; }

    //// 魔力
    //public float Fascination { get; set; }
    //public float MaxFascination { get; set; }

    //// 興奮度
    //public float Excitement { get; set; }
    //public float MaxExcitement { get; set; }

    //// 情慾度
    //public float Lust { get; set; }
    //public float MaxLust { get; set; }

    //// 苦難 煎熬度
    //public float Excruciation { get; set; }
    //public float MaxExcruciation { get; set; }

    //// 從順度
    //public float Obedience { get; set; }
    //public float MaxObedience { get; set; }

    // 戰鬥中的距離
    public DistanceAssign Distance { get; set; }

    public enum DistanceAssign
    {
        near, middle, away
    }

    public enum DistanceChange
    {
        none = 0, near = -1, nearDouble = -2, away = 1, awayDouble = 2
    }
}
