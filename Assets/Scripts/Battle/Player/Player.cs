﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : IPlayer
{

    protected float _vitality = 8000;
    private DistanceAssign _distance;

    public float Vitality
    {
        get
        {
            return _vitality;
        }

        set
        {
            if (_vitality != value)
            {
                InvokeHPChange(_vitality - value);
                _vitality = Mathf.Clamp(value, 0, value);
            }
        }
    }

    public DistanceAssign Distance
    {
        get
        {
            return _distance;
        }
        set
        {
            _distance = value;
        }
    }


    private void InvokeHPChange(float value)
    {
        FindObjectOfType<BattleMessageDisplayer>().AddMessage("主角____受到了" + value + "點傷害!");
    }

    void Awake()
    {
        Distance = DistanceAssign.away;
    }
}
