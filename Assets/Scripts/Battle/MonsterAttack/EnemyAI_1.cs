﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI_1 : IMonsterAI
{

    MSkill_0 skill_0;
    MSkill_2 skill_2;

    public EnemyAI_1()
    {
        skill_0 = new MSkill_0();
        skill_2 = new MSkill_2();
    }

    public MonsterSkillInheritance EnemyAITurn()
    {
        GameManager _gameManager = GameManager.Settings;
        MonsterSkillInheritance monsterSkill = null;

        Enemy enemy = _gameManager.GetEnemy();
        if (enemy.HP == enemy.MaxHP)
        {
            monsterSkill = skill_0;
        }
        else
        {
            monsterSkill = skill_2;
        }

        return monsterSkill;
    }
}
