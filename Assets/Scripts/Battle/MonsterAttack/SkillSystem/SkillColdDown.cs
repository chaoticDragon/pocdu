﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillColdDown : MonoBehaviour
{

    float _coldDown = 0f;
    float _skillPrepareStart = 0f;
    bool _isSkillActivate = false;
    private MonsterSkillInheritance _nextSkill;

    public Slider slider;


    // Use this for initialization
    void Start()
    {
        slider = GetComponent<Slider>();

        //TestSkill();
    }

    public void MonoUpdate()
    {
        if (_isSkillActivate)
        {
            _coldDown += Time.deltaTime * Time.timeScale;
            slider.value = (_coldDown / _skillPrepareStart);
            if (_coldDown >= _skillPrepareStart)
            {
                TrySkillActive();
            }
        }
    }

    public void TestSkill()
    {
        _skillPrepareStart = 4.0f;
        _isSkillActivate = true;
        _coldDown = 0f;
    }

    public void PutSkill(float skillPrepareStart, MonsterSkillInheritance skill)
    {
        _skillPrepareStart = skillPrepareStart;
        _nextSkill = skill;
        _isSkillActivate = true;
        _coldDown = 0f;
    }

    private void TrySkillActive()
    {
        if (_nextSkill != null)
        {
            _nextSkill.ActiveSkill();
            GameManager.Settings.GameStatus = GameStatus.Status.enemyTurn;
            //_nextSkill.ActiveSkill();
        }
    }
}
