﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMonsterSkill 
{

    string SkillName { get; set; }
    float SkillColdDown { get; set; }
    float NearDamage { get; set; }
    float MiddleDamage { get; set; }
    float FarDamage { get; set; }
    float BasePower { get; set; }
    bool IsAttack { get; set; }

   

}
