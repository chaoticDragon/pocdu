﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MonsterSkillInheritance: IMonsterSkill
{
    protected string _skillName;
    protected float _skillColdDown;
    protected float _nearDamage;
    protected float _middleDamage;
    protected float _farDamage;
    protected float _basePower;
    protected bool _isAttack;

    public string SkillName
    {
        get
        {
            return _skillName;
        }

        set
        {
            _skillName = value;
        }
    }

    public float SkillColdDown
    {
        get
        {
            return _skillColdDown;
        }

        set
        {
            _skillColdDown = value;
        }
    }

    public float NearDamage
    {
        get
        {
            return _nearDamage;
        }

        set
        {
            _nearDamage = value;
        }
    }

    public float MiddleDamage
    {
        get
        {
            return _middleDamage;
        }

        set
        {
            _middleDamage = value;
        }
    }

    public float FarDamage
    {
        get
        {
            return _farDamage;
        }

        set
        {
            _farDamage = value;
        }
    }

    public float BasePower
    {
        get
        {
            return _basePower;
        }

        set
        {
            _basePower = value;
        }
    }

    public bool IsAttack
    {
        get
        {
            return _isAttack;
        }

        set
        {
            _isAttack = value;
        }
    }

    public abstract void ActiveSkill();

    //public void ActiveSkill()
    //{
    //    SkillColdDown skillColdDown = FindObjectOfType<SkillColdDown>();
    //    skillColdDown.PutSkill(SkillColdDown, this);
    //}
}
