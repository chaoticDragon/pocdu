﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterChangePosition
{

    public void Change(IPlayer.DistanceChange position)
    {
        GameManager game = GameManager.Settings;
        int change = (int)position;
        int nowDistance = (int)game.GetPlayer().Distance;
        nowDistance += change;
        if (nowDistance <= 0)
        {
            game.GetPlayer().Distance = IPlayer.DistanceAssign.near;
        }
        else if (nowDistance == 1)
        {
            game.GetPlayer().Distance = IPlayer.DistanceAssign.middle;
        }
        else if (nowDistance >= 1)
        {
            game.GetPlayer().Distance = IPlayer.DistanceAssign.away;
        }
    }

    public void Assign(IPlayer.DistanceAssign position)
    {
        GameManager game = GameManager.Settings;
        game.GetPlayer().Distance = position;
    }
}
