﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterTakenDamage
{

    public void DamageToPlayer(int power)
    {
        GameManager game = GameManager.Settings;
        game.GetPlayer().Vitality = game.GetPlayer().Vitality - power;
    }

    //public void ObsceneToxinDamageToPlayer(int power)
    //{
    //    GameManager game = GameManager.Settings;
    //    game.GetEnemy().ObsceneToxin = power;
    //}
}
