﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MSkill_1 : MonsterSkillInheritance
{
    public MSkill_1()
    {
        SkillName = "抓擊";
        SkillColdDown = 4f;
        NearDamage = 100f;
        MiddleDamage = 0f;
        FarDamage = 0f;
        BasePower = 1000;
        IsAttack = true;
    }

    public override void ActiveSkill()
    {
        throw new System.NotImplementedException();
    }
}
