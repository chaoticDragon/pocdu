﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MSkill_0 : MonsterSkillInheritance
{
    public MSkill_0()
    {
        _skillName = "只是遠處觀察";
        _skillColdDown = 2f;
        _nearDamage = 0f;
        _middleDamage = 0f;
        _farDamage = 0f;
        _basePower = 1000;
        _isAttack = false;
    }

    public override void ActiveSkill()
    {
        //SkillColdDown skillColdDown = FindObjectOfType<SkillColdDown>();
        //skillColdDown.PutSkill(SkillColdDown, this);
    }
}
