﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MSkill_2 : MonsterSkillInheritance
{
    MonsterChangePosition monsterChangePosition;
    MonsterTakenDamage takenDamage;
    public MSkill_2()
    {
        SkillName = "撞擊";
        SkillColdDown = 4f;
        NearDamage = 100f;
        MiddleDamage = 60f;
        FarDamage = 40f;
        BasePower = 1000;
        IsAttack = true;

        monsterChangePosition = new MonsterChangePosition();
        takenDamage = new MonsterTakenDamage();
    }

    public override void ActiveSkill()
    {
        float trueDamage =  BattleUtil.GetEnemyAttack(this);
        takenDamage.DamageToPlayer((int)trueDamage);
        monsterChangePosition.Change(IPlayer.DistanceChange.near);
    }
}
