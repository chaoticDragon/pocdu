﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMonsterAI  {

    MonsterSkillInheritance EnemyAITurn();
}
