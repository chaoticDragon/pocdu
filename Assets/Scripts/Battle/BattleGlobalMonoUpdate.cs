﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * 將所有對於戰鬥中會運作的比如UI管理或者回合推演時
 * 會控制到需要在by frames為單位的判斷的事情
 * 都放置在這裡執行以避免大量的分別的update拖慢效能
 */ 
public class BattleGlobalMonoUpdate : MonoBehaviour {

    SkillColdDown skillColdDown;
    ButtonCardSlider[] buttonCardSliders;

    // Use this for initialization
    void Start () {
        skillColdDown = FindObjectOfType<SkillColdDown>();
        buttonCardSliders = FindObjectsOfType<ButtonCardSlider>();
    }

    // Update is called once per frame
    void Update () {
        skillColdDown.MonoUpdate();
        for (int i = 0; i < buttonCardSliders.Length; i++)
        {
            buttonCardSliders[i].MonoUpdate();
        }
    }
}
