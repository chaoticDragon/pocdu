﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleSenceInit : MonoBehaviour
{
    readonly int target = 30;
    private void Reset()
    {

    }

    private void Awake()
    {
        Application.targetFrameRate = target;

        GameManager.Init();
        print("game init");
    }

    // Use this for initialization
    void Start()
    {
        StartCoroutine(InitGame());
    }

    IEnumerator InitGame()
    {
        yield return new WaitForEndOfFrame();
        BattleUtil.SkillsInit();
        GameManager.Settings.GameStatus = GameStatus.Status.enemyTurn;
    }
}
