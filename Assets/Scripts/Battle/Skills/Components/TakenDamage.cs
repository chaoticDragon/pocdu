﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakenDamage
{

    public void Damage(int power)
    {
        GameManager game = GameManager.Settings;
        game.GetEnemy().HP = power;
    }

    public void ObsceneToxinDamage(int power)
    {
        GameManager game = GameManager.Settings;
        game.GetEnemy().ObsceneToxin = power;
    }
}
