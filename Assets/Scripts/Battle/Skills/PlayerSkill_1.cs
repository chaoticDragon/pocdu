﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkill_1 : SkillGeneric
{

    TakenDamage damage = null;
    ChangePosition changePosition = null;

    public PlayerSkill_1()
    {
        damage = new TakenDamage();
        changePosition = new ChangePosition();
        _skillName = "魔法刃";
        _skillColdDown = 2.0f;
        _power = 1000;
}

public override void Action()
    {
        damage.Damage(_power);
        changePosition.Assign(IPlayer.DistanceAssign.near);
    }
}
