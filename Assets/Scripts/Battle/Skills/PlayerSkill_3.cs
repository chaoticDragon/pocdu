﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkill_3 : SkillGeneric
{

    TakenDamage damage = null;
    ChangePosition changePosition = null;

    public PlayerSkill_3()
    {
        damage = new TakenDamage();
        changePosition = new ChangePosition();
        _skillName = "魔砲";
        _skillColdDown = 8.0f;
        _power = 2500;
    }

    public override void Action()
    {
        damage.Damage(_power);
        changePosition.Assign(IPlayer.DistanceAssign.away);
    }
}
