﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkill_2 : SkillGeneric
{

    TakenDamage damage = null;
    ChangePosition changePosition = null;

    public PlayerSkill_2()
    {
        damage = new TakenDamage();
        changePosition = new ChangePosition();
        _skillName = "狙擊";
        _skillColdDown = 4.0f;
        _power = 600;
    }

    public override void Action()
    {
        damage.Damage(_power);
        changePosition.Assign(IPlayer.DistanceAssign.away);
    }
}
