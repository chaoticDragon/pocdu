﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SkillGeneric : ISkill
{
    protected string _skillName;
    protected float _skillColdDown;
    protected int _power;

    public string SkillName
    {
        get
        {
            return _skillName;
        }

        set
        {
            _skillName = value;
        }
    }

    public float SkillColdDown
    {
        get
        {
            return _skillColdDown;
        }

        set
        {
            _skillColdDown = value;
        }
    }

    public int Power
    {
        get
        {
            return _power;
        }

        set
        {
            _power = value;
        }
    }

    public abstract void Action();
}
