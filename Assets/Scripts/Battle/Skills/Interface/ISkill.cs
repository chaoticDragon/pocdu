﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISkill
{
    string SkillName { get; set; }
    float SkillColdDown { get; set; }
    int Power { get; set; }

}
