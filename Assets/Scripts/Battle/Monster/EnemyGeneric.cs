﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGeneric : ScriptableObject, IStatusObject
{
    public event EventHandler HPChanged;
    public event EventHandler MaxHPChanged;

    public BattleMessageDisplayer messageDisplayer;

    protected float _hp = 0;
    [SerializeField]
    protected float _maxHP = 4500;

    protected float _mp = 0;
    protected float _maxMp = 10000;
    protected float _obsceneToxin = 0;
    protected float _maxObsceneToxin = 10000;

    public IMonsterAI MonsterAI { get; set; }

    public float HP
    {
        get
        {
            return _hp;
        }

        set
        {
            FindObjectOfType<BattleMessageDisplayer>().AddMessage("魔物____受到了" + value + "點傷害!");
            value = _hp - value;
            value = Mathf.Clamp(value, 0, _maxHP);
            if (_hp != value)
            {
                _hp = value;
                this.InvokeHPChanged();
            }
        }
    }

    private void InvokeHPChanged()
    {
        if (HPChanged != null)
        {
            HPChanged(this, EventArgs.Empty);
        }
    }

    public float MaxHP
    {
        get { return _maxHP; }
        set
        {
            value = Mathf.Clamp(value, 1, float.PositiveInfinity);
            if (_maxHP != value)
            {
                _maxHP = value;
                //this.InvokeMaxHPChanged();
                this.HP = Mathf.Clamp(_hp, 0, _maxHP);
            }
        }
    }

    public float MP
    {
        get
        {
            throw new NotImplementedException();
        }

        set
        {
            throw new NotImplementedException();
        }
    }

    public float MaxMP
    {
        get { return _maxMp; }
        set
        {
            value = Mathf.Clamp(value, 1, float.PositiveInfinity);
            if (_maxMp != value)
            {
                _maxMp = value;
                //this.InvokeMaxHPChanged();
                //this.HP = Mathf.Clamp(_hp, 0, _maxHP);
            }
        }
    }

    public float ObsceneToxin
    {
        get
        {
            return _obsceneToxin;
        }

        set
        {
            _obsceneToxin = value;
        }
    }

    public float MaxObsceneToxin
    {
        get
        {
            return _maxObsceneToxin;
        }

        set
        {
            _maxObsceneToxin = value;
        }
    }

    //public float MaxMP { get => _maxMp; set => _maxMp = value; }
    //public float ObsceneToxin { get => _obsceneToxin; set => _obsceneToxin = value; }
    //public float MaxObsceneToxin { get => _maxObsceneToxin; set => _maxObsceneToxin = value; }

    //public Enemy()
    //{
    //    Awake();
    //}

    void Awake()
    {
        Debug.Log("enemy awake BEFORE HP: " + _hp);

        _hp = _maxHP;
        Debug.Log("enemy awake");
    }

    void Update()
    {
        //var inst = this.hpObject as IStatusObject;
        //if (inst != null)
        //{
        //    var hp = inst.HP;
        //    var maxHp = inst.MaxHP;
        //    var percentage = Mathf.InverseLerp(0, maxHp, hp);
        //    this.uiText.text = string.Format("HP Percentage: {0:.2f}", percentage);
        //}
    }
}
