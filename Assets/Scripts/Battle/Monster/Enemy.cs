﻿using System;
using UnityEngine;

public class Enemy : EnemyGeneric
{

    void Awake()
    {
        MonsterAI = new EnemyAI_1();

        _maxHP = 4500;

        _hp = _maxHP;
    }
}
