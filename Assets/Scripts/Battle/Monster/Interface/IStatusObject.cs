﻿public interface IStatusObject
{
    float HP { get; set; }
    float MaxHP { get; set; }

    float MP { get; set; }
    float MaxMP { get; set; }

    float ObsceneToxin { get; set; }
    float MaxObsceneToxin { get; set; }


}