﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHPSlider : MonoBehaviour {

    public Slider slider;

    private void Awake()
    {
        slider = GetComponent<Slider>();
    }

    // Use this for initialization
    void Start () {
        Enemy enemy = GameManager.Settings.GetEnemy();
        enemy.HPChanged += this.OnPlayerHurted;

    }
	
	// Update is called once per frame
	void Update () {
    }

    //計算按鈕的點擊事件
    void ButtonClick(GameObject button)
    {
    }

    void OnPlayerHurted(object sender, EventArgs e)
    {
        // TODO:
        Enemy enemy = GameManager.Settings.GetEnemy();
        slider.value = ((float)enemy.HP / enemy.MaxHP);

    }
}
