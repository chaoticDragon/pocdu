﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
 * AP殘餘量顯示的地方 目前暫被當作HP顯示使用
 */
public class ParameterChange : MonoBehaviour {


    Text text;

    public int ap = 30;

    void Awake()
    {
        text = GetComponent<Text>();
    }

    void Start () {
        Enemy enemy = GameManager.Settings.GetEnemy();
        enemy.HPChanged += this.OnPlayerHurted;
    }

    void Update () {
		
	}

    void OnPlayerHurted(object sender, EventArgs e)
    {
        Enemy enemy = GameManager.Settings.GetEnemy();
        text.text = enemy.HP.ToString();
    }
}
