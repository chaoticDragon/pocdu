﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NewBehaviourScript : MonoBehaviour {

    public Text text;
    private float speed;

    // Use this for initialization
    void Start () {
        text = GetComponent<Text>();
        text.text = "ready to click";
        speed = 1.0f;
    }

    // Update is called once per frame
    void Update () {
        float step = speed * Time.deltaTime;
        gameObject.transform.localPosition = 
            new Vector3(Mathf.Lerp(gameObject.transform.localPosition.x, 10, step), Mathf.Lerp(gameObject.transform.localPosition.y, -3, step), Mathf.Lerp(gameObject.transform.localPosition.z, 50, step));//插值算法也可以

    }

    void OnMouseEnter()
    {
        text.text = "onClick!";
    }
}
