﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageEvent1 : MonoBehaviour {

    Image myImageComponent;

    // Use this for initialization
    void Start () {
        myImageComponent = GetComponent<Image>();
        myImageComponent.sprite = Resources.Load<Sprite>("p002");

    }

    // Update is called once per frame
    void Update () {

    }
}
